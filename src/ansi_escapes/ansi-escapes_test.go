package ansi_escapes

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCursorTo(t *testing.T) {
	assert.Equal(t, CursorToXY(2, 2), "\u001b[3;3H")
}
